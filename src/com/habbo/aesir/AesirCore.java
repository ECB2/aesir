package com.habbo.aesir;

import com.habbo.aesir.encryption.EncryptionHandler;
import com.habbo.aesir.encryption.Keys;
import com.habbo.aesir.messages.Packets;
import com.habbo.aesir.network.AesirServer;
import com.habbo.aesir.network.SessionManager;
import com.habbo.aesir.util.StringUtil;
import io.netty.util.CharsetUtil;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

/**
 * Created by ECB2 on 16.02.2016.
 */
public class AesirCore {

    private Logger _Logger;
    private Properties _Config;
    private Connection _Database;
    private AesirServer _AesirSrv;
    private SessionManager _SessionMgr;
    private Packets _PacketMgr;
    private static AesirCore _Self;
    private String _PolicyFile;

    public static void main(String[] args) throws Exception {
        new AesirCore().run();
    }

    public AesirCore() {
        this._Self = this;
        this._Logger = LogManager.getLogger("Aesir");
        this._Config = new Properties();
        this._SessionMgr = new SessionManager();
    }

    public void run() throws Exception {
        this._Logger.info("Launching Aesir...");

        this._PolicyFile = new String(Files.readAllBytes(Paths.get("policy.xml")), CharsetUtil.UTF_8);

        if(!new File("habbodata.json").exists())
        {
            this._Logger.fatal("habbodata.json was not found! Aesir will not start without it.");
            return;
        }else{
            this._PacketMgr = new Packets();
            this._PacketMgr.LoadPacketIDs("habbodata.json");
            this._PacketMgr.AddMessages();
            this._Logger.info("Loaded Packets!");
        }

        if(!new File("aesir.cfg").exists())
        {
            this._Config.setProperty("game.host", "0.0.0.0");
            this._Config.setProperty("game.port", "30000");
            this._Config.setProperty("mysql.hostname", "localhost");
            this._Config.setProperty("mysql.username", "root");
            this._Config.setProperty("mysql.password", "p@s5w0rd");
            this._Config.setProperty("mysql.database", "aesir");

            String[] _tmpComments = {"Aesir Emulator by f00lish_m0rtal", "Repository: http://bitbucket.org/ECB2/aesir", "", "Generated on:"};

            this._Config.store(new FileWriter("aesir.cfg"), StringUtil.join(_tmpComments, "\r\n"));
            this._Logger.warn("aesir.cfg has been created. Please fill out the required fields and restart aesir.");
            return;
        }else{
            this._Config.load(new FileReader("aesir.cfg"));
        }

        String MySQLHostname = this._Config.getProperty("mysql.hostname");
        String MySQLUsername = this._Config.getProperty("mysql.username");
        String MySQLPassword = this._Config.getProperty("mysql.password");
        String MySQLDatabase = this._Config.getProperty("mysql.database");

        this._Logger.info("Establishing database connection...");
        try{
            this._Database = DriverManager.getConnection(String.format("jdbc:mysql://%s:3306/%s", MySQLHostname, MySQLDatabase), MySQLUsername, MySQLPassword);

            Statement stmt = _Database.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT `value` FROM aesir_settings WHERE `key`='name'");
            rs.next();
            this._Logger.info("Connected to database! Launching Hotel: " + rs.getString(1));
            stmt.close();
        }catch(Exception e){
            this._Logger.error(e.getMessage());
            this._Logger.error("Could not connect to database. Quitting.");
        }

        this._Logger.info("Setting up EncryptionHandler...");
        this._PacketMgr.SetupEncryptionHandler("habbodata.json");
        this._Logger.info("EncryptionHandler has been set up!");

        this._Logger.info("Setting up ConnectionHandler...");

        this._AesirSrv = new AesirServer(
                this,
                this._Config.getProperty("game.host"),
                Integer.parseInt(this._Config.getProperty("game.port"))
        );
        this._AesirSrv.run();

        this._Logger.info("ConnectionHandler has been set up!");

    }

    public SessionManager SessionManager()
    {
        return this._SessionMgr;
    }

    public static AesirCore self()
    {
        return _Self;
    }

    public static Short PacketOut(String xName)
    {
        return self()._PacketMgr.GetOutgoingPacketHeader(xName);
    }
    public static Short PacketIn(String xName)
    {
        return self()._PacketMgr.GetIncomingPacketHeader(xName);
    }

    public Logger getLogger()
    {
        return this._Logger;
    }

    public Packets Packets()
    {
        return this._PacketMgr;
    }

    public Connection Database() { return this._Database; }

    public static String GetPolicy() {
        return self()._PolicyFile;
    }
}
