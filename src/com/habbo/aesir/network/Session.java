package com.habbo.aesir.network;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.encryption.ARC4;
import com.habbo.aesir.encryption.DiffieHellman;
import com.habbo.aesir.messages.PacketComposer;
import com.habbo.aesir.messages.outgoing.NavigatorSettingsMessageComposer;
import com.habbo.aesir.messages.outgoing.UserRightsMessageComposer;
import com.habbo.aesir.messages.outgoing.handshake.AuthenticationOKMessageComposer;
import com.habbo.aesir.messages.outgoing.handshake.SetUniqueIdMessageComposer;
import com.habbo.aesir.objects.HabboUser;
import io.netty.channel.Channel;

import java.net.InetSocketAddress;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by Pascal on 21.02.2016.
 */
public class Session {

    private Channel _Channel;
    private ARC4 _ARC4;
    private DiffieHellman _DH;

    private String _MachineId;
    private String _OperatingSystem;
    private String _SSO;

    private HabboUser _HabboUser;

    public Session(Channel xChannel)
    {
        this._DH = new DiffieHellman();
        this._Channel = xChannel;
    }

    public void initARC4()
    {
        this._ARC4 = new ARC4(this.getDH().getSharedKey().toByteArray());
    }

    public Channel getChannel()     { return this._Channel; }
    public ARC4 getARC4()           { return this._ARC4; }
    public DiffieHellman getDH()    { return this._DH; }

    public void sendPacket(PacketComposer xPacket)
    {
        System.out.println("Sending " + AesirCore.self().Packets().GetOutgoingPacketName(xPacket.Header()));
        xPacket.flush(this._Channel );
    }

    /**
     * Sets the machine id allocated to the client.
     * @param xMachineId The machine id of the client.
     */
    public void setMachineId(String xMachineId) { this._MachineId = xMachineId; }

    /**
     * Sets the operating system the client is using.
     * @param xOS The operating system the client is using.
     */
    public void setOperatingSystem(String xOS) { this._OperatingSystem = xOS; }

    /**
     * Sets the SSO the client is providing.
     * @param xSSO The SSO ticket the client provided.
     */
    public void setSSO(String xSSO) { this._SSO = xSSO; }


    /**
     * Tries to login the user, by verifying his SSO ticket.
     */
    public void login()
    {
        try{
            AesirCore pAesir = AesirCore.self();
            PreparedStatement pStatement = pAesir.Database().prepareStatement("SELECT `id`,`username`,`figure` FROM users WHERE `sso_ticket` IS NOT NULL AND NOT `sso_ticket`='' AND `sso_ticket`=? LIMIT 1");
            pStatement.setString(1, this._SSO);
            pStatement.execute();

            int pID;
            String pUsername, pFigure;
            ResultSet pRS = pStatement.getResultSet();
            while(pRS.next())
            {
                pID = pRS.getInt("id");
                pUsername = pRS.getString("username");
                pFigure = pRS.getString("figure");
                InetSocketAddress pAddr = (InetSocketAddress) this.getChannel().remoteAddress();
                AesirCore.self().getLogger().info(pUsername + " logged in from " + pAddr.getHostString()+"(" + pAddr.getHostName()+")!");
                pRS.close();
                pStatement.close();
                this._HabboUser = new HabboUser(pID, pUsername, this);
                this._HabboUser.setFigure(pFigure);
                this.sendPacket(new SetUniqueIdMessageComposer(this._MachineId).Compose());
                this.sendPacket(new AuthenticationOKMessageComposer().Compose());
                this.sendPacket(new NavigatorSettingsMessageComposer(0).Compose());
                this.sendPacket(new UserRightsMessageComposer(7).Compose());
                this._HabboUser.LoadStats();
                return;
            }
            // Connection didn't succeed, invalid SSO or some other shit. Signed by Cpt. Obvious
            pRS.close();
            pStatement.close();
            return;

        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public HabboUser getHabbo()
    {
        return this._HabboUser;
    }

    /**
     * Disconnects a user gracefully.
     */
    public void Disconnect()
    {
        //Fettsack hier musst du den User kicken und darauf achten das nichts verloren geht.
    }

}
