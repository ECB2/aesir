package com.habbo.aesir.network;

import io.netty.buffer.ByteBuf;

/**
 * Created by Pascal on 24.02.2016.
 */
public class WrappedBuffer {

    private ByteBuf _Buffer;

    /**
     * Initiates a WrappedBuffer object
     * @param xBuffer - The ByteBuf that should be wrapped
     */
    public WrappedBuffer(ByteBuf xBuffer)
    {
        this._Buffer = xBuffer;
    }

    /**
     * Reads a string from the ByteBuf.
     * @return Read string
     */
    public String readString()
    {
        byte[] pBuffer = new byte[this.readShort()];
        this._Buffer.readBytes(pBuffer);
        try{
            return new String(pBuffer, "UTF-8");
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Reads a boolean from the ByteBuf.
     * @return Read boolean
     */
    public Boolean readBoolean()
    {
        return this.readByte() == 1;
    }

    /**
     * Reads a integer from the ByteBuf.
     * @return Read integer
     */
    public Integer readInt()
    {
        return this._Buffer.readInt();
    }

    /**
     * Reads a short from the ByteBuf.
     * @return Read short
     */
    public Short readShort()
    {
        return this._Buffer.readShort();
    }

    /**
     * Reads a byte from the ByteBuf.
     * @return Read byte
     */
    public Byte readByte()
    {
        return this._Buffer.readByte();
    }

    /**
     * Reads a specific amount of bytes from the ByteBuf.
     * @param xLength - The size of the array
     * @return Read array
     */
    public byte[] readBytes(int xLength)
    {
        byte[] pBuffer = new byte[xLength];
        _Buffer.readBytes(pBuffer);
        return pBuffer;
    }

}
