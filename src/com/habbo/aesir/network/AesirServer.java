package com.habbo.aesir.network;

import com.habbo.aesir.AesirCore;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * Created by Pascal on 20.02.2016.
 */
public class AesirServer {

    private AesirServer _Aesir;
    private String _Host;
    private int _Port;

    public AesirServer(AesirCore xAesir, String xHost, int xPort)
    {
        this._Host = xHost;
        this._Port = xPort;
    }

    public void run() throws Exception
    {
        EventLoopGroup pBossGroup = new NioEventLoopGroup(1);
        EventLoopGroup pWorkerGroup = new NioEventLoopGroup(10);

        try{
            ServerBootstrap pBootstrap = new ServerBootstrap();
            pBootstrap.group(pBossGroup, pWorkerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast("messageDecoder", (ChannelHandler) new AesirMessageDecoder());
                            ch.pipeline().addLast(new AesirConnectionHandler());
                        }
                    })
                    .childOption(ChannelOption.TCP_NODELAY, true)
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .childOption(ChannelOption.SO_REUSEADDR, true)
                    .childOption(ChannelOption.SO_RCVBUF, 5120)
                    .childOption(ChannelOption.RCVBUF_ALLOCATOR, new FixedRecvByteBufAllocator(5120))
                    .childOption(ChannelOption.ALLOCATOR, new PooledByteBufAllocator());

            ChannelFuture pFuture = pBootstrap.bind(this._Host, this._Port);

            pFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //Bootstrap
            pWorkerGroup.shutdownGracefully();
            pBossGroup.shutdownGracefully();
        }
    }

}
