package com.habbo.aesir.network;

import io.netty.channel.Channel;
import io.netty.channel.socket.SocketChannel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pascal on 21.02.2016.
 */
public class SessionManager {

    private List<Session> _Sessions;

    public SessionManager()
    {
        this._Sessions = new ArrayList<Session>();
    }

    public Session RegisterSession(Channel xChannel)
    {
        for(Session pS : this._Sessions)
            if(pS.getChannel() == xChannel)
                return null;

        Session pSession = new Session(xChannel);
        this._Sessions.add(pSession);
        return pSession;
    }

    public void UnregisterSession(Channel xChannel)
    {
        for(Session pS : this._Sessions)
            if(pS.getChannel() == xChannel)
            {
                pS.Disconnect();
                this._Sessions.remove(pS);
                break;
            }
    }

    public Session GetSessionByChannel(Channel xChannel)
    {
        for(Session pS : this._Sessions)
            if(pS.getChannel() == xChannel)
                return pS;
        return null;
    }

}
