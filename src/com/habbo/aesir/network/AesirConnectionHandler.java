package com.habbo.aesir.network;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.Packets;
import com.habbo.aesir.messages.incoming.handshake.GetClientVersionMessageEvent;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;
import io.netty.util.ReferenceCountUtil;
import org.apache.logging.log4j.Logger;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Pascal on 20.02.2016.
 */
public class AesirConnectionHandler extends ChannelInboundHandlerAdapter{

    private String _PolicyFile;
    private AesirCore _Aesir;
    private Logger _Logger;
    private Session _Session;

    public AesirConnectionHandler()
    {
        try{
            this._PolicyFile = new String(Files.readAllBytes(Paths.get("policy.xml")), CharsetUtil.UTF_8);
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
        this._Aesir = AesirCore.self();
        this._Logger = _Aesir.getLogger();
    }

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception
    {
        this._Session = _Aesir.SessionManager().RegisterSession(ctx.channel());
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception
    {
        _Aesir.SessionManager().UnregisterSession(ctx.channel());
        this._Session = null;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception
    {
        ByteBuf buffer = (ByteBuf)msg;
        while( buffer.readableBytes() != 0){
            int length = buffer.readInt();
            readMessage(ctx, buffer.readBytes(length));
        }
    }

    public void readMessage(ChannelHandlerContext ctx, ByteBuf in)
    {
        in.markReaderIndex();
        Short header = in.readShort();
        if(this._Aesir.Packets().GetIncomingPacketName(header) != null)
        {
            this._Logger.info("[" + this._Aesir.Packets().GetIncomingPacketName(header) + "] Received Packet!");
            this._Aesir.Packets().HandlePacket(this._Session, header, new WrappedBuffer(in));
        }else{
            this._Logger.info("Unknown packet received! (ID: " + header + ")");
        }
        in.resetReaderIndex();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
    {
        cause.printStackTrace();
        ctx.close();
        return;
    }

}
