package com.habbo.aesir.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.*;

/**
 * Created by Pascal on 25.02.2016.
 */

/**
 * Marks a variable as a variable that will be read from the packet, all variables have to be in the right order.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface HabboMessageParameter {

    public enum ParameterType
    {
        STRING, BOOLEAN, INT, BYTE, BYTE_ARRAY
    }

    ParameterType Type();
    int Length() default 0;

}
