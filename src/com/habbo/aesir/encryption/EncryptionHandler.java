package com.habbo.aesir.encryption;

import com.google.gson.JsonObject;

import java.util.Properties;

/**
 * Created by Pascal on 19.02.2016.
 */
public class EncryptionHandler {

    private RSA _RSA;
    private Keys _Keys;

    public EncryptionHandler(JsonObject xJSON)
    {
        this._Keys = new Keys(xJSON);
        this._RSA = new RSA();
        this._RSA.init(this._Keys);
    }

    public RSA RSA() { return this._RSA; }
    public Keys Keys() { return this._Keys; }

}
