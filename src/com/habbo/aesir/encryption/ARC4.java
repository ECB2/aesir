package com.habbo.aesir.encryption;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

/**
 * ARC4 Implementation
 *
 * Original by Joopie1994 - http://github.com/Joopie1994/Encryption/blob/master/Hurlant/Crypto/Prng/ARC4.cs
 *
 * @author f00lish_m0rtal
 * Created by f00lish_m0rtal on 19.02.2016.
 */
public class ARC4 {

    public int POOLSIZE = 256;
    private int i;
    private int j;
    private byte[] bytes;


    public ARC4()
    {
        bytes = new byte[POOLSIZE];
    }

    public ARC4(byte[] key)
    {
        bytes = new byte[POOLSIZE];
        this.Initialize(key);
    }

    public void Initialize(byte[] key)
    {
        this.i = 0;
        this.j = 0;

        for (i = 0; i < POOLSIZE; ++i) {
            this.bytes[i] = (byte) i;
        }

        for (i = 0; i < POOLSIZE; ++i) {
            j = (j + bytes[i] + key[i % key.length]) & (POOLSIZE - 1);
            Swap(i, j);
        }

        this.i = 0;
        this.j = 0;
    }

    private void Swap(int a, int b)
    {
        byte t = this.bytes[a];
        this.bytes[a] = this.bytes[b];
        this.bytes[b] = t;
    }

    private byte Next()
    {
        this.i = ++this.i & (POOLSIZE - 1);
        this.j = (this.j + this.bytes[i]) & (POOLSIZE - 1);
        this.Swap(i, j);
        return this.bytes[(this.bytes[i] + this.bytes[j]) & 255];
    }

    public ByteBuf Encrypt(ByteBuf src)
    {
        ByteBuf pRet = Unpooled.buffer();
        while(src.isReadable())
            pRet.writeByte((byte) (src.readByte() ^ this.Next()));

        return pRet;
    }


}
