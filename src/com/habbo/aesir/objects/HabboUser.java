package com.habbo.aesir.objects;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.outgoing.user.inventory.ActivityPointsMessageComposer;
import com.habbo.aesir.messages.outgoing.user.inventory.CreditBalanceMessageComposer;
import com.habbo.aesir.network.Session;

import java.sql.*;

/**
 * Created by Pascal on 26.02.2016.
 */
public class HabboUser {

    private int     _Id;
    private String  _Username;
    private String  _Figure;

    private Session _Session;
    private int     _Credits;
    private int     _Duckets;
    private int     _Diamonds;

    public HabboUser(int xId, String xUsername, Session xSession)
    {
        this._Id = xId;
        this._Username = xUsername;
        this._Session = xSession;
    }

    public int getId()
    {
        return this._Id;
    }

    public String getUsername()
    {
        return this._Username;
    }

    public String getFigure()
    {
        return this._Figure;
    }

    public String getGender()
    {
        return "m";
    }

    public void setFigure(String xFigure)
    {
        this._Figure = xFigure;
    }

    public void LoadStats()
    {
        try {
            Connection pDatabase = AesirCore.self().Database();
            PreparedStatement pStatistics = null;
            pStatistics = pDatabase.prepareStatement("SELECT `diamonds`,`credits`,`duckets` FROM users WHERE `id`=?");
            pStatistics.setInt(1, this._Id);
            pStatistics.execute();
            ResultSet pRS = pStatistics.getResultSet();
            pRS.next();
            this._Diamonds = pRS.getInt("diamonds");
            this._Credits  = pRS.getInt("credits");
            this._Duckets  = pRS.getInt("duckets");
            pRS.close();
            pStatistics.close();
            AesirCore.self().getLogger().info(String.format("%s has %d credits, %d duckets and %d diamonds.", this._Username, this._Credits, this._Duckets, this._Diamonds));

            this._Session.sendPacket(new CreditBalanceMessageComposer(this._Credits).Compose());
            this._Session.sendPacket(new ActivityPointsMessageComposer(this._Duckets, this._Diamonds).Compose());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
