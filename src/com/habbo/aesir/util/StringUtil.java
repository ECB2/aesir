package com.habbo.aesir.util;

/**
 * Created by ECB2 on 17.02.2016.
 */
public class StringUtil {

    public static String join(String[] xArray, String xJoinChar)
    {
        String _tmpString = "";
        for(int i = 0; i < xArray.length; i++) _tmpString += xArray[i] + ((i!=xArray.length-1)?xJoinChar:"");
        return _tmpString;
    }

}
