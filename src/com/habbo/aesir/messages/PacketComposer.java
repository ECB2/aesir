package com.habbo.aesir.messages;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;

import java.io.IOException;

/**
 * Created by Pascal on 25.02.2016.
 */
public class PacketComposer {

    private ByteBuf _Buffer;
    private ByteBufOutputStream _BufferStream;
    private short _Header;

    public PacketComposer(short xHeader) throws IOException
    {
        this._Header = xHeader;
        this._Buffer = Unpooled.buffer();
        this._BufferStream = new ByteBufOutputStream(this._Buffer);
        this._BufferStream.writeInt(0);
        this._BufferStream.writeShort(xHeader);
    }

    public short Header()
    {
        return this._Header;
    }

    public void writeShort(short xShort) throws IOException
    {
        this._BufferStream.writeShort(xShort);
    }

    public void writeInt(int xInteger) throws IOException
    {
        this._BufferStream.writeInt(xInteger);
    }

    public void writeInt(boolean xBoolean) throws IOException
    {
        this._BufferStream.writeInt(xBoolean ? 1 : 0);
    }

    public void writeBoolean(boolean xBoolean) throws IOException
    {
        this._BufferStream.writeBoolean(xBoolean);
    }

    public void writeLong(long xLong) throws IOException
    {
        this._BufferStream.writeLong(xLong);
    }

    public void writeString(String xString) throws IOException
    {
        this._BufferStream.writeUTF(xString);
        System.out.println("str: " + xString);
    }

    public void writeByte(byte xByte) throws IOException
    {
        this._BufferStream.writeByte(xByte);
    }

    public void flush(Channel xChannel)
    {
        this._Buffer.setInt(0, this._Buffer.writerIndex() - 4);
        xChannel.write(this._Buffer.copy(), xChannel.voidPromise());
        xChannel.flush();
    }

}
