package com.habbo.aesir.messages;

import java.io.IOException;

/**
 * Created by Pascal on 25.02.2016.
 */
public interface ComposerBase {

    public PacketComposer Compose() throws IOException;

}
