package com.habbo.aesir.messages.outgoing.handshake;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.ComposerBase;
import com.habbo.aesir.messages.PacketComposer;

import java.io.IOException;

/**
 * Created by Pascal on 27.02.2016.
 */
public class AuthenticationOKMessageComposer implements ComposerBase {
    @Override
    public PacketComposer Compose() throws IOException {
        PacketComposer _Composer = new PacketComposer(AesirCore.PacketOut("AuthenticationOKMessageComposer"));
        return _Composer;
    }
}
