package com.habbo.aesir.messages.outgoing.handshake;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.ComposerBase;
import com.habbo.aesir.messages.PacketComposer;

import java.io.IOException;

/**
 * Created by Pascal on 27.02.2016.
 */
public class SetUniqueIdMessageComposer implements ComposerBase {

    private String _UniqueID;

    public SetUniqueIdMessageComposer(String xUniqueID)
    {
        this._UniqueID = xUniqueID;
    }

    @Override
    public PacketComposer Compose() throws IOException {
        PacketComposer _Composer = new PacketComposer(AesirCore.PacketOut("SetUniqueIdMessageComposer"));
        _Composer.writeString(this._UniqueID);
        return _Composer;
    }
}
