package com.habbo.aesir.messages.outgoing.handshake;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.ComposerBase;
import com.habbo.aesir.messages.PacketComposer;
import com.habbo.aesir.network.Session;

import java.io.IOException;

/**
 * Created by Pascal on 26.02.2016.
 */
public class SecretKeyMessageComposer implements ComposerBase{

    private String _PrivateKey;

    public SecretKeyMessageComposer(String xPrivateKey)
    {
        this._PrivateKey = xPrivateKey;
    }

    @Override
    public PacketComposer Compose() throws IOException {
        PacketComposer _Composer = new PacketComposer(AesirCore.PacketOut("SecretKeyMessageComposer"));
        _Composer.writeString(this._PrivateKey);
        _Composer.writeBoolean(false); //Should we encrypt Server -> Client communication?
        return _Composer;
    }
}
