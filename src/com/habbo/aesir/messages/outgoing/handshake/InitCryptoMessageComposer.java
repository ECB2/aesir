package com.habbo.aesir.messages.outgoing.handshake;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.ComposerBase;
import com.habbo.aesir.messages.PacketComposer;

import java.io.IOException;

/**
 * Created by Pascal on 25.02.2016.
 */
public class InitCryptoMessageComposer implements ComposerBase{

    private String _Prime;
    private String _Generator;

    public InitCryptoMessageComposer(String xPrime, String xGenerator) throws IOException {
        this._Prime = xPrime;
        this._Generator = xGenerator;
    }

    @Override
    public PacketComposer Compose() throws IOException {
        PacketComposer _Composer = new PacketComposer(AesirCore.PacketOut("InitCryptoMessageComposer"));
        _Composer.writeString(this._Prime);
        _Composer.writeString(this._Generator);
        return _Composer;
}
}
