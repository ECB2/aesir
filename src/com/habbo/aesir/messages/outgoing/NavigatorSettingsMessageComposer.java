package com.habbo.aesir.messages.outgoing;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.ComposerBase;
import com.habbo.aesir.messages.PacketComposer;

import java.io.IOException;

/**
 * Created by Pascal on 27.02.2016.
 */
public class NavigatorSettingsMessageComposer implements ComposerBase {

    private int _Room;
    public NavigatorSettingsMessageComposer(int xRoom)
    {
        this._Room = xRoom;
    }

    @Override
    public PacketComposer Compose() throws IOException {
        PacketComposer _Composer = new PacketComposer(AesirCore.PacketOut("NavigatorSettingsMessageComposer"));
        _Composer.writeInt(this._Room);
        _Composer.writeInt(this._Room);
        return _Composer;
    }
}
