package com.habbo.aesir.messages.outgoing;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.ComposerBase;
import com.habbo.aesir.messages.PacketComposer;

import java.io.IOException;

/**
 * Created by Pascal on 27.02.2016.
 */
public class UserRightsMessageComposer implements ComposerBase {

    private int _Rank;

    public UserRightsMessageComposer(int xRank)
    {
        this._Rank = xRank;
    }

    @Override
    public PacketComposer Compose() throws IOException {
        PacketComposer _Composer = new PacketComposer(AesirCore.PacketOut("UserRightsMessageComposer"));
        _Composer.writeInt(2);
        _Composer.writeInt(this._Rank);
        _Composer.writeInt(0);
        return _Composer;
    }
}
