package com.habbo.aesir.messages.outgoing.user.inventory;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.ComposerBase;
import com.habbo.aesir.messages.PacketComposer;

import java.io.IOException;

/**
 * Created by Pascal on 27.02.2016.
 */
public class ActivityPointsMessageComposer implements ComposerBase {

    private int _Duckets;
    private int _Diamonds;

    public ActivityPointsMessageComposer(int xDuckets, int xDiamonds)
    {
        this._Diamonds = xDiamonds;
        this._Duckets = xDuckets;
    }

    @Override
    public PacketComposer Compose() throws IOException {
        PacketComposer _Composer = new PacketComposer(AesirCore.PacketOut("ActivityPointsMessageComposer"));
        _Composer.writeInt(2);
        _Composer.writeInt(0);
        _Composer.writeInt(this._Duckets);
        _Composer.writeInt(5);
        _Composer.writeInt(this._Diamonds);
        return _Composer;
    }
}
