package com.habbo.aesir.messages.outgoing.user.inventory;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.ComposerBase;
import com.habbo.aesir.messages.PacketComposer;

import java.io.IOException;

/**
 * Created by Pascal on 28.02.2016.
 */
public class CreditBalanceMessageComposer implements ComposerBase {

    private int _Credits;

    public CreditBalanceMessageComposer(int xCredits)
    {
        this._Credits = xCredits;
    }


    public PacketComposer Compose() throws IOException {
        PacketComposer pComposer = new PacketComposer(AesirCore.PacketOut("CreditBalanceMessageComposer"));
        pComposer.writeString(this._Credits + ".0");
        return pComposer;
    }
}
