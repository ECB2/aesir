package com.habbo.aesir.messages.outgoing.user;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.ComposerBase;
import com.habbo.aesir.messages.PacketComposer;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.objects.HabboUser;

import java.io.IOException;

/**
 * Created by Pascal on 28.02.2016.
 */
public class UserObjectMessageComposer implements ComposerBase {

    private Session _Session;

    public UserObjectMessageComposer(Session xSession)
    {
        this._Session = xSession;
    }

    @Override
    public PacketComposer Compose() {
        try {
            HabboUser pHabbo = this._Session.getHabbo();
            PacketComposer pComposer = new PacketComposer(AesirCore.PacketOut("UserObjectMessageComposer"));
            pComposer.writeString(String.valueOf(pHabbo.getId() + 9));
            pComposer.writeString(pHabbo.getUsername());
            pComposer.writeString(pHabbo.getFigure());
            pComposer.writeString(pHabbo.getGender().toUpperCase());
            pComposer.writeString("Ja man");
            pComposer.writeString(""); //realname
            pComposer.writeBoolean(false);
            pComposer.writeInt(0);
            pComposer.writeInt(3);
            pComposer.writeInt(3);
            pComposer.writeBoolean(false); //friends stream
            pComposer.writeString("1456633840");
            pComposer.writeBoolean(false);
            pComposer.writeBoolean(false);
            return pComposer;
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }
}
