package com.habbo.aesir.messages.outgoing.user;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.ComposerBase;
import com.habbo.aesir.messages.PacketComposer;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.objects.HabboUser;

import java.io.IOException;

/**
 * Created by Pascal on 28.02.2016.
 */
public class UserPerksMessageComposer implements ComposerBase {

    private Session _Session;

    public UserPerksMessageComposer(Session xSession)
    {
        this._Session = xSession;
    }

    @Override
    public PacketComposer Compose() {
        try {
            HabboUser pHabbo = this._Session.getHabbo();
            PacketComposer pComposer = new PacketComposer(AesirCore.PacketOut("UserPerksMessageComposer"));
            pComposer.writeInt(11);
            pComposer.writeString("BUILDERS_AT_WORK");
            pComposer.writeString("");
            pComposer.writeBoolean(true);//can use floor editor?
            pComposer.writeString("VOTE_IN_COMPETITIONS");
            pComposer.writeString("requirement.unfulfilled.helper_level_2");
            pComposer.writeBoolean(false);
            pComposer.writeString("USE_GUIDE_TOOL");
            pComposer.writeString("");
            pComposer.writeBoolean(true);
            pComposer.writeString("JUDGE_CHAT_REVIEWS");
            pComposer.writeString("");
            pComposer.writeBoolean(true);
            pComposer.writeString("NAVIGATOR_ROOM_THUMBNAIL_CAMERA");
            pComposer.writeString("");
            pComposer.writeBoolean(true);
            pComposer.writeString("CALL_ON_HELPERS");
            pComposer.writeString("");
            pComposer.writeBoolean(true);
            pComposer.writeString("CITIZEN");
            pComposer.writeString("");
            pComposer.writeBoolean(true);
            pComposer.writeString("MOUSE_ZOOM");
            pComposer.writeString("");
            pComposer.writeBoolean(false);
            pComposer.writeString("TRADE");
            pComposer.writeString("");
            pComposer.writeBoolean(true);
            pComposer.writeString("CAMERA");
            pComposer.writeString("");
            pComposer.writeBoolean(true);
            pComposer.writeString("NAVIGATOR_PHASE_TWO_2014");
            pComposer.writeString("");
            pComposer.writeBoolean(true);
            return pComposer;
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }
}
