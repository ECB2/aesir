package com.habbo.aesir.messages.outgoing.user.avatar;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.ComposerBase;
import com.habbo.aesir.messages.PacketComposer;
import com.habbo.aesir.objects.HabboUser;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by Pascal on 28.02.2016.
 */
public class WardrobeMessageComposer implements ComposerBase {

    private HabboUser _Habbo;

    public WardrobeMessageComposer(HabboUser xHabbo)
    {
        this._Habbo = xHabbo;
    }

    /*
    structure:
    int - count
    for(i < count)
        int - slot
        str - figure
        str - gender
     */
    public PacketComposer Compose() throws IOException {
        try{
            PacketComposer pComposer = new PacketComposer(AesirCore.PacketOut("WardrobeMessageComposer"));

            pComposer.writeInt(1);

            PreparedStatement pHasWardrobe = AesirCore.self().Database().prepareStatement("SELECT COUNT(*) AS cnt FROM `users_wardrobe` WHERE `user_id`=?");
            pHasWardrobe.setInt(1, this._Habbo.getId());
            ResultSet pDrobeCountRS = pHasWardrobe.executeQuery();
            pDrobeCountRS.next();
            int pLookCount = pDrobeCountRS.getInt("cnt");
            pDrobeCountRS.close();
            pHasWardrobe.close();

            pComposer.writeInt(pLookCount);

            if(pLookCount > 0) {
                PreparedStatement pGetWardrobe = AesirCore.self().Database().prepareStatement("SELECT * FROM `users_wardrobe` WHERE `user_id`=?");
                pGetWardrobe.setInt(1, this._Habbo.getId());
                ResultSet pRS = pGetWardrobe.executeQuery();

                while (pRS.next()) {
                    pComposer.writeInt(pRS.getInt("slot"));
                    pComposer.writeString(pRS.getString("figure"));
                    pComposer.writeString(pRS.getString("gender").toUpperCase() );
                }
                pRS.close();
                pGetWardrobe.close();
            }

            return pComposer;
        }catch(Exception ex)
        {
            return null;
        }
    }
}
