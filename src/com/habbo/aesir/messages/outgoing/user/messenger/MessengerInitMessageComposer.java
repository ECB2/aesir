package com.habbo.aesir.messages.outgoing.user.messenger;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.messages.ComposerBase;
import com.habbo.aesir.messages.PacketComposer;

import java.io.IOException;

/**
 * Created by Pascal on 28.02.2016.
 */
public class MessengerInitMessageComposer implements ComposerBase {
    @Override
    public PacketComposer Compose() throws IOException {
        PacketComposer pComposer = new PacketComposer(AesirCore.PacketOut("MessengerInitMessageComposer"));
        pComposer.writeInt(100);
        pComposer.writeInt(300);
        pComposer.writeInt(800);
        pComposer.writeInt(1100);
        pComposer.writeInt(0);
        return pComposer;
    }
}
