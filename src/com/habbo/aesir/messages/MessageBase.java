package com.habbo.aesir.messages;

import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

/**
 * Created by Pascal on 22.02.2016.
 */
public interface MessageBase {

    /**
     * Handles a message passed to the server by the client.
     * @param xSession - The clients session
     * @param xBuffer - The ByteBuf wrapped as a WrappedBuffer passed to the server by the client.
     */
    void handle(Session xSession, WrappedBuffer xBuffer);

}
