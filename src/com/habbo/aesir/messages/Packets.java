package com.habbo.aesir.messages;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.habbo.aesir.AesirCore;
import com.habbo.aesir.annotations.HabboMessageParameter;
import com.habbo.aesir.encryption.EncryptionHandler;
import com.habbo.aesir.messages.incoming.NullRouteEvent;
import com.habbo.aesir.messages.incoming.handshake.*;
import com.habbo.aesir.messages.incoming.user.GetCreditsInfoMessageEvent;
import com.habbo.aesir.messages.incoming.user.InfoRetrieveMessageEvent;
import com.habbo.aesir.messages.incoming.user.avatar.GetWardrobeMessageEvent;
import com.habbo.aesir.messages.incoming.user.avatar.SaveWardrobeOutfitMessageEvent;
import com.habbo.aesir.messages.incoming.user.avatar.UpdateFigureDataMessageEvent;
import com.habbo.aesir.messages.incoming.user.messenger.MessengerInitMessageEvent;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * Created by Pascal on 22.02.2016.
 */
public class Packets {

    private HashMap<Short, String> _IncomingIDs;
    private HashMap<Short, Class<?>> _IncomingHandlers;
    private HashMap<Short, String> _OutgoingIDs;
    private EncryptionHandler _Encryption;

    public Packets() {
        this._IncomingIDs = new HashMap<Short, String>();
        this._IncomingHandlers = new HashMap<Short, Class<?>>();
        this._OutgoingIDs = new HashMap<Short, String>();
    }

    public void SetupEncryptionHandler(String xDataFile) throws FileNotFoundException
    {
        JsonObject pHabboData = new JsonParser().parse(new FileReader(xDataFile)).getAsJsonObject();
        this._Encryption = new EncryptionHandler(pHabboData);
    }

    public EncryptionHandler Encryption()
    {
        return this._Encryption;
    }

    public String GetIncomingPacketName(Short xID)
    {
        if(!this._IncomingIDs.containsKey(xID)) return null;
        return this._IncomingIDs.get(xID);
    }

    public Short GetIncomingPacketHeader(String xName)
    {
        if(!this._IncomingIDs.containsValue(xName)) return null;
        for(Entry<Short, String> pTmpEntry : this._IncomingIDs.entrySet())
            if(pTmpEntry.getValue().equals(xName))
                return pTmpEntry.getKey();
        return null;
    }

    public String GetOutgoingPacketName(Short xID)
    {
        if(!this._OutgoingIDs.containsKey(xID)) return null;
        return this._OutgoingIDs.get(xID);
    }

    public Short GetOutgoingPacketHeader(String xName)
    {
        if(!this._OutgoingIDs.containsValue(xName)) return null;
        for(Entry<Short, String> pTmpEntry : this._OutgoingIDs.entrySet())
            if(pTmpEntry.getValue().equals(xName))
                return pTmpEntry.getKey();
        return null;
    }

    public void AddMessages()
    {
        /** Handshake packets */
        AddMessage("GetClientVersionMessageEvent", GetClientVersionMessageEvent.class);
        AddMessage("InitCryptoMessageEvent", InitCryptoMessageEvent.class);
        AddMessage("GenerateSecretKeyMessageEvent", GenerateSecretKeyMessageEvent.class);
        AddMessage("UniqueIDMessageEvent", UniqueIDMessageEvent.class);
        AddMessage("ClientVariablesMessageEvent", ClientVariablesMessageEvent.class);
        AddMessage("SSOTicketMessageEvent", SSOTicketMessageEvent.class);

        /** Users */
        AddMessage("InfoRetrieveMessageEvent", InfoRetrieveMessageEvent.class);
        AddMessage("GetCreditsInfoMessageEvent", GetCreditsInfoMessageEvent.class);

        /** Users -> Funny figure meme xd */
        AddMessage("UpdateFigureDataMessageEvent", UpdateFigureDataMessageEvent.class);
        AddMessage("SaveWardrobeOutfitMessageEvent", SaveWardrobeOutfitMessageEvent.class);
        AddMessage("GetWardrobeMessageEvent", GetWardrobeMessageEvent.class);

        /** Messenger */
        AddMessage("MessengerInitMessageEvent", MessengerInitMessageEvent.class);

        /** Null Routes!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
        AddMessage("MemoryPerformanceMessageEvent", NullRouteEvent.class);
    }

    public void AddMessage(String xName, Class xHandler)
    {
        this._IncomingHandlers.put(this.GetIncomingPacketHeader(xName), xHandler);
    }

    public void HandlePacket(Session xSession, Short xHeader, WrappedBuffer xBuffer)
    {
        if(this._IncomingHandlers.containsKey(xHeader))
        {
            try{
                Class _tmpMsgClass = this._IncomingHandlers.get(xHeader);
                MessageBase _tmpMsg = (MessageBase) _tmpMsgClass.newInstance();
                for(Field _tmpField:_tmpMsg.getClass().getDeclaredFields())
                {
                    if(_tmpField.isAnnotationPresent(HabboMessageParameter.class))
                    {
                        HabboMessageParameter _tmpParamAnnotation = _tmpField.getAnnotation(HabboMessageParameter.class);
                        _tmpField.setAccessible(true);
                        switch(_tmpParamAnnotation.Type()) {
                            case STRING:
                                _tmpField.set(_tmpMsg, xBuffer.readString());
                                break;
                            case BOOLEAN:
                                _tmpField.set(_tmpMsg, xBuffer.readBoolean());
                                break;
                            case INT:
                                _tmpField.set(_tmpMsg, xBuffer.readInt());
                                break;
                            case BYTE:
                                _tmpField.set(_tmpMsg, xBuffer.readByte());
                                break;
                            case BYTE_ARRAY:
                                _tmpField.set(_tmpMsg, xBuffer.readBytes(_tmpParamAnnotation.Length()));
                                break;
                        }
                    }
                }
                _tmpMsg.handle(xSession, xBuffer);
            }catch(Exception ex)
            {
                ex.printStackTrace();
            }
        }else{
            AesirCore.self().getLogger().warn("No handler for " + xHeader + (this._IncomingIDs.containsKey(xHeader)?" (" + this._IncomingIDs.get(xHeader) + ")":"") + " defined!");
        }
    }

    public void LoadPacketIDs(String xDataFile) throws FileNotFoundException {
        JsonObject pHabboData = new JsonParser().parse(new FileReader(xDataFile)).getAsJsonObject();

        JsonObject pPacketsIncoming = pHabboData.get("packets").getAsJsonObject().get("incoming").getAsJsonObject();
        for(Entry<String, JsonElement> pTmpObj : pPacketsIncoming.entrySet())
        {
            _IncomingIDs.put(pTmpObj.getValue().getAsShort(), pTmpObj.getKey());
        }

        JsonObject pPacketsOutgoing = pHabboData.get("packets").getAsJsonObject().get("outgoing").getAsJsonObject();
        for(Entry<String, JsonElement> pTmpObj : pPacketsOutgoing.entrySet())
        {
            _OutgoingIDs.put(pTmpObj.getValue().getAsShort(), pTmpObj.getKey());
        }
    }

}
