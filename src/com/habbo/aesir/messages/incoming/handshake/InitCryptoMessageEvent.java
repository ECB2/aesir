package com.habbo.aesir.messages.incoming.handshake;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.encryption.RSA;
import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.messages.outgoing.handshake.InitCryptoMessageComposer;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

/**
 * Created by Pascal on 26.02.2016.
 */
public class InitCryptoMessageEvent implements MessageBase {

    @Override
    public void handle(Session xSession, WrappedBuffer xBuffer) {
        try{
            RSA pRSA = AesirCore.self().Packets().Encryption().RSA();
            InitCryptoMessageComposer _Composer = new InitCryptoMessageComposer(
                    pRSA.sign(xSession.getDH().getPrime().toString()).toUpperCase(),
                    pRSA.sign(xSession.getDH().getGenerator().toString()).toUpperCase());
            xSession.sendPacket(_Composer.Compose());
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
