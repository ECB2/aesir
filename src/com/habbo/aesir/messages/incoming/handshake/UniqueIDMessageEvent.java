package com.habbo.aesir.messages.incoming.handshake;

import com.habbo.aesir.annotations.HabboMessageParameter;
import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.messages.PacketComposer;
import com.habbo.aesir.messages.outgoing.handshake.SetUniqueIdMessageComposer;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

import java.io.IOException;

/**
 * Created by Pascal on 26.02.2016.
 */
public class UniqueIDMessageEvent implements MessageBase {

    @HabboMessageParameter(Type = HabboMessageParameter.ParameterType.STRING)
    private String _DummyString;
    @HabboMessageParameter(Type = HabboMessageParameter.ParameterType.STRING)
    private String _MachineID;
    @HabboMessageParameter(Type = HabboMessageParameter.ParameterType.STRING)
    private String _OS;

    @Override
    public void handle(Session xSession, WrappedBuffer xBuffer) {
        xSession.setMachineId(this._MachineID);
        xSession.setOperatingSystem(this._OS);
        try {
            xSession.sendPacket(new SetUniqueIdMessageComposer(this._MachineID).Compose());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
