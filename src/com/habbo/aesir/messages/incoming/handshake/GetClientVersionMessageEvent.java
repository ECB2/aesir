package com.habbo.aesir.messages.incoming.handshake;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.annotations.HabboMessageParameter;
import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

/**
 * Created by ECB2 on 24.02.2016.
 */
public class GetClientVersionMessageEvent implements MessageBase {

    /**
     * The version the client is requesting to use.
     */
    @HabboMessageParameter(Type = HabboMessageParameter.ParameterType.STRING)
    private String _Version;

    /**
     * The environment the user is requesting to use. Should be "FLASH" in all cases
     */
    @HabboMessageParameter(Type = HabboMessageParameter.ParameterType.STRING)
    private String _Type;

    public void handle(Session xSession, WrappedBuffer xBuffer)
    {
        AesirCore.self().getLogger().info("V: " + _Version + ", T: " + _Type);
    }
}
