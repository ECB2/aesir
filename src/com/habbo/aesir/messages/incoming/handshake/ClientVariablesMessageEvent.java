package com.habbo.aesir.messages.incoming.handshake;

import com.habbo.aesir.annotations.HabboMessageParameter;
import com.habbo.aesir.annotations.HabboMessageParameter.ParameterType;
import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

/**
 * Created by Pascal on 26.02.2016.
 */
public class ClientVariablesMessageEvent implements MessageBase {

    @HabboMessageParameter(Type = ParameterType.STRING)
    private String _Unk1;

    @Override
    public void handle(Session xSession, WrappedBuffer xBuffer) {

    }
}
