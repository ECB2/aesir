package com.habbo.aesir.messages.incoming.handshake;

import com.habbo.aesir.annotations.HabboMessageParameter;
import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

/**
 * Created by Pascal on 26.02.2016.
 */
public class SSOTicketMessageEvent implements MessageBase {

    @HabboMessageParameter(Type = HabboMessageParameter.ParameterType.STRING)
    private String _SSO;

    @Override
    public void handle(Session xSession, WrappedBuffer xBuffer) {
        xSession.setSSO(this._SSO);
        xSession.login();
    }
}
