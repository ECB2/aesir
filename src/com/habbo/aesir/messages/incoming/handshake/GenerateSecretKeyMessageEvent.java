package com.habbo.aesir.messages.incoming.handshake;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.annotations.HabboMessageParameter;
import com.habbo.aesir.encryption.RSA;
import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.messages.outgoing.handshake.SecretKeyMessageComposer;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

/**
 * Created by Pascal on 26.02.2016.
 */
public class GenerateSecretKeyMessageEvent implements MessageBase {

    /**
     * The public cipher key supplied by the client.
     */
    @HabboMessageParameter(Type= HabboMessageParameter.ParameterType.STRING)
    private String _CipherPublicKey;

    @Override
    public void handle(Session xSession, WrappedBuffer xBuffer) {
        try{
            RSA pRSA = AesirCore.self().Packets().Encryption().RSA();
            String pKey = pRSA.decrypt(this._CipherPublicKey).replace(String.valueOf((char)0), "");
            SecretKeyMessageComposer pComposer = new SecretKeyMessageComposer(pRSA.sign(xSession.getDH().getPublicKey().toString()));
            xSession.sendPacket(pComposer.Compose());
            xSession.getDH().generateSharedKey(pKey);
            xSession.initARC4();
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
