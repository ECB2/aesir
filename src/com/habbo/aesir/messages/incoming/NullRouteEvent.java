package com.habbo.aesir.messages.incoming;

import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

/**
 * Created by Pascal on 26.02.2016.
 */
public class NullRouteEvent implements MessageBase {
    @Override
    public void handle(Session xSession, WrappedBuffer xBuffer) { }
}
