package com.habbo.aesir.messages.incoming.user.messenger;

import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.messages.PacketComposer;
import com.habbo.aesir.messages.outgoing.user.messenger.MessengerInitMessageComposer;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

import java.io.IOException;

/**
 * Created by Pascal on 28.02.2016.
 */
public class MessengerInitMessageEvent implements MessageBase {
    @Override
    public void handle(Session xSession, WrappedBuffer xBuffer) {
        try {
            xSession.sendPacket(new MessengerInitMessageComposer().Compose());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
