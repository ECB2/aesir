package com.habbo.aesir.messages.incoming.user.avatar;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.annotations.HabboMessageParameter;
import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Created by Pascal on 28.02.2016.
 */
public class SaveWardrobeOutfitMessageEvent implements MessageBase {

    @HabboMessageParameter(Type= HabboMessageParameter.ParameterType.INT)
    private int _Slot;
    @HabboMessageParameter(Type= HabboMessageParameter.ParameterType.STRING)
    private String _Figure;
    @HabboMessageParameter(Type= HabboMessageParameter.ParameterType.STRING)
    private String _Gender;

    public void handle(Session xSession, WrappedBuffer xBuffer) {
        try{
            this._Gender = this._Gender.toUpperCase();

            if(!_Gender.equals("M") && !_Gender.equals("F")) return;

            Connection pConnection = AesirCore.self().Database();
            PreparedStatement pWardrobeSlotTaken = pConnection.prepareStatement("SELECT null FROM `users_wardrobe` WHERE `user_id`=? AND `slot`=?");
            pWardrobeSlotTaken.setInt(1, xSession.getHabbo().getId());
            pWardrobeSlotTaken.setInt(2, this._Slot);
            pWardrobeSlotTaken.executeQuery();

            if(pWardrobeSlotTaken.getResultSet().next())
            {
                //Slot is already assigned

                PreparedStatement pUpdate = pConnection.prepareStatement("UPDATE `users_wardrobe` SET `figure`=?,`gender`=? WHERE `slot`=? AND `user_id`=?");
                pUpdate.setString(1, this._Figure);
                pUpdate.setString(2, this._Gender);
                pUpdate.setInt(3, this._Slot);
                pUpdate.setInt(4, xSession.getHabbo().getId());
                pUpdate.executeUpdate();
                pUpdate.close();
            }else{
                //Slot needs to be assigned

                PreparedStatement pUpdate = pConnection.prepareStatement("INSERT INTO `users_wardrobe` VALUES (?,?,?,?)");
                pUpdate.setInt(1, xSession.getHabbo().getId());
                pUpdate.setInt(2, this._Slot);
                pUpdate.setString(3, this._Gender);
                pUpdate.setString(4, this._Figure);
                pUpdate.executeUpdate();
                pUpdate.close();
            }
            pWardrobeSlotTaken.getResultSet().close();
            pWardrobeSlotTaken.close();
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }

    }
}
