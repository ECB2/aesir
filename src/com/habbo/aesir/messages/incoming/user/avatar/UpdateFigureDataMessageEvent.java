package com.habbo.aesir.messages.incoming.user.avatar;

import com.habbo.aesir.AesirCore;
import com.habbo.aesir.annotations.HabboMessageParameter;
import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Pascal on 28.02.2016.
 */
public class UpdateFigureDataMessageEvent implements MessageBase {

    @HabboMessageParameter(Type = HabboMessageParameter.ParameterType.STRING)
    private String _Gender;
    @HabboMessageParameter(Type = HabboMessageParameter.ParameterType.STRING)
    private String _Figure;

    @Override
    public void handle(Session xSession, WrappedBuffer xBuffer) {
        try {
            PreparedStatement pStatement = AesirCore.self().Database().prepareStatement("UPDATE users SET figure=?, gender=? WHERE id=?");
            pStatement.setString(1, this._Figure);
            pStatement.setString(2, this._Gender);
            pStatement.setInt(3, xSession.getHabbo().getId());
            pStatement.execute();
            pStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
