package com.habbo.aesir.messages.incoming.user.avatar;

import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.messages.outgoing.user.avatar.WardrobeMessageComposer;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

/**
 * Created by Pascal on 28.02.2016.
 */
public class GetWardrobeMessageEvent implements MessageBase {

    public void handle(Session xSession, WrappedBuffer xBuffer) {
        try{
            xSession.sendPacket(new WardrobeMessageComposer(xSession.getHabbo()).Compose());
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
