package com.habbo.aesir.messages.incoming.user;

import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.messages.outgoing.UserRightsMessageComposer;
import com.habbo.aesir.messages.outgoing.user.UserObjectMessageComposer;
import com.habbo.aesir.messages.outgoing.user.UserPerksMessageComposer;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;

import java.io.IOException;

/**
 * Created by Pascal on 28.02.2016.
 */
public class InfoRetrieveMessageEvent implements MessageBase {


    @Override
    public void handle(Session xSession, WrappedBuffer xBuffer) {
        xSession.sendPacket(new UserObjectMessageComposer(xSession).Compose());
        try {
            xSession.sendPacket(new UserRightsMessageComposer(9).Compose());
        } catch (IOException e) {
            e.printStackTrace();
        }
        xSession.sendPacket(new UserPerksMessageComposer(xSession).Compose());
    }
}
