package com.habbo.aesir.messages.incoming.user;

import com.habbo.aesir.messages.MessageBase;
import com.habbo.aesir.network.Session;
import com.habbo.aesir.network.WrappedBuffer;
import com.habbo.aesir.objects.HabboUser;

/**
 * Created by Pascal on 28.02.2016.
 */
public class GetCreditsInfoMessageEvent implements MessageBase {

    public void handle(Session xSession, WrappedBuffer xBuffer) {
        HabboUser _Habbo = xSession.getHabbo();
        _Habbo.LoadStats();
    }
}
