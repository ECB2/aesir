package com.habbo.aesir.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 *
 * @author f00lish_m0rtal
 * Created by f00lish_m0rtal on 19.02.2016.
 */
public class ConnectionHandler implements Runnable{

    private String          _BindAddr = "0.0.0.0";
    private int             _Port = 30000;
    protected boolean       _IsStopped = false;
    protected Thread        _RunningThread = null;
    protected ServerSocket  _ServerSocket = null;

    public ConnectionHandler(String xBindAddr, int xPort)
    {
        this._BindAddr = xBindAddr;
        this._Port = xPort;
    }

    public void run()
    {
        synchronized (this){
            this._RunningThread = Thread.currentThread();
        }
        this._StartListen();
        while(!_IsStopped())
        {
            Socket _Client = null;
            try{
                _Client = this._ServerSocket.accept();
                System.out.println("New connection from " + _Client.getInetAddress().getHostAddress() + ":" + _Client.getPort());
            }catch(IOException e)
            {
                e.printStackTrace();
            }
            new Thread(new ConnectionHandlerSlave(_Client)).start();
        }
    }

    private synchronized boolean _IsStopped()
    {
        return this._IsStopped;
    }

    private synchronized void _Stop()
    {
        this._IsStopped = true;
        try{
            this._ServerSocket.close();
        }catch(IOException e)
        {
            throw new RuntimeException("Could not close server", e);
        }
    }

    private void _StartListen()
    {
        try{
            this._ServerSocket = new ServerSocket(this._Port);
        }catch(IOException e)
        {
            throw new RuntimeException("Could not run ServerSocket on port " + this._Port, e);
        }
    }

}
