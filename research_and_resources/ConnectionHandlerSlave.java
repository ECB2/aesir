package com.habbo.aesir.network;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by ECB2 on 19.02.2016.
 */
public class ConnectionHandlerSlave implements Runnable {

    protected Socket _Client;
    private String _PolicyFile = "";

    public ConnectionHandlerSlave(Socket xClient)
    {
        this._Client = xClient;
        try{
            this._PolicyFile = new String(Files.readAllBytes(Paths.get("policy.xml")), Charset.defaultCharset());
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void run()
    {
        try{
            InputStream pInput = _Client.getInputStream();
            OutputStream pOutput = _Client.getOutputStream();
            BufferedReader pInputReader = new BufferedReader(new InputStreamReader(pInput));
            BufferedWriter pOutputWriter = new BufferedWriter(new OutputStreamWriter(pOutput));

            while(_Client.isConnected())
            {
                try{
                    if(pInput.available()==0) continue;
                    String _Packet = new String(pInputReader.readLine());
                    System.out.println("[PACKET] " + _Packet);

                    if(_Packet.startsWith("<policy-file-request/>"))
                    {
                        System.out.println("Replying to request..");
                        pOutputWriter.append(_Packet);
                        pOutputWriter.flush();

                    }
                }catch(Exception ex)
                {
                    ex.printStackTrace();
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readToEnd(InputStream in) throws IOException {
        byte[] b = new byte[1024];
        int n;
        StringBuilder sb = new StringBuilder();
        while ((n = in.read(b)) >= 0) {
            sb.append(b);
        }
        return sb.toString();
    }

}
