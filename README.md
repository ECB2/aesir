## What is Aesir? ##
Aesir is a Habbo Hotel Emulator written in Java.  
It is by no means perfect, nor is it meant to be used in a production environment.  
It is purely a project that I can work on in my free time whenever I get interested in it.  
  
That's also the reason why this repository is private, if for any reason you have access to it, you are somehow trusted by me.  
I ask you to not abuse the trust I'm putting into you, so please do NOT share anything you see in here.  

**Current Version:** *PRODUCTION-201602082203-712976078*
  
Thanks!